# Summary

Proof of concept implementation of using Vue.js to render Foundry `Application`s instead of Handlebars. It's a normal
 Foundry module, so just copy the `vuejs` folder to `public/modules` and use the `VueJSSheet` for an actor.

The goal was to be able to replace the implementation of `Application` without any backwards-incompatibilities, i.e
. if the original classes are replaced with these new ones, everything should still behave exactly the same. 

This
 works and can be verified by replacing `public/scripts/foundry.js` and `public/systems/dnd5e/dnd5e.js` with the
  [`foundry.js`](https://gitlab.com/foundry-azzurite/vuejs-poc/blob/master/vuejs/foundry.js) and [`dnd5e.js`](https://gitlab.com/foundry-azzurite/vuejs-poc/blob/master/vuejs/dnd5e.js) files
   contained in this repository. Those two files simply have their classes
   replaced with the new `ModularABC` classes.

# Implementation

Notes about the implementation can be found inside the js files, marked by the string `NEW_MODULAR_RENDERER`, just
 search the files for this.
 
 On a high level, the `Application` class simply receives another parameter in its constructor, a `renderer`. The
  renderer should be provided by an implementation of `Application`. This
  renderer is used instead of the `_renderInner` method. That's basically it.
  
# Performance tests

To test for performance, read the code comment in `main.js`, search for `PERFORMANCE TESTS`.

Animations for the `Application` popout windows should be removed for this, instructions for that are also in the
 code comment.
 
The available performance tests are:

* **Initial rendering:** Opens and closes the sheet again and again (while clearing the app cache)
* **Update rendering:** Keeps the sheet open and changes the HP of the actors from 0 to 1 and back

On my machine, I had the following results on a new actor with 200 loops:
 

| Renderer | Initial rendering | Update rendering |
|---|---|---|
|**Handlebars**|3648.2 ms|3112.2 ms|
|**Vue.js**|3151.4 ms|1313.7 ms|

which means the average time for any individual render would be

| Renderer | Initial rendering | Update rendering |
|---|---|---|
|**Handlebars**|18.241 ms|15.561 ms|
|**Vue.js**|15.757 ms|6.569 ms|

That means Vue.js initial rendering is 15.7% faster than handlebars, and updates are 136.9% faster. Or in other words
 (depending on which way you like more)
, Vue.js initial rendering only takes 86.4% of the time of the Handlebars rendering, and 42.2% of the time for update
 rendering.
 
 Bonus statistic: doing the drop in replacement also reduces the update rendering of Handlebars from 3112.2ms to 2329
 .9 ms, a 33.5% improvement!

I performed each test 5 times, the above numbers are the average of those, with the following individual results:

**Handlebars + Initial**  
3648.305ms  
3616.917ms  
3658.067ms  
3668.586ms  
3648.960ms

**Vue.js + Initial**  
3162.129ms  
3212.762ms  
3220.378ms  
3085.272ms  
3076.211ms

**Handlebars + Update**  
3148.563ms  
3149.232ms  
3187.887ms  
3041.189ms  
3034.338ms

**Vue.js + Update**  
1246.948ms  
1398.933ms  
1408.993ms  
1270.582ms  
1243.137ms

It has to be said though, that there are two quite obvious optimizations available for Vue.js still:

* Instead of a method like `activateListeners`, that runs on Vue.js HTML after rendering, attach the listeners
 through the standard Vue way of creating components and adding listeners directly in their template(s)
* Instead of using a method like `getData`(which returns a completely new JS object) while rendering, update the
 properties in the data model of Vue individually (i.e. you would just do something like `actor.attributes.hp.value
  = 12;` and this would automatically change the display of it as well)
