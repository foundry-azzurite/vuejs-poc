'use strict';


/**
 * NEW_MODULAR_RENDERER
 *
 * Unchanged, just here to fit into the class hierarchy
 */
class ModularBaseEntitySheet extends ModularFormApplication {
	constructor(...args) {
		super(...args);

		// Register the sheet as an active Application for the Entity
		this.entity.apps[this.appId] = this;
	}

	/* -------------------------------------------- */

	/**
	 * A convenience accessor for the object property of the inherited FormApplication instance
	 */
	get entity() {
		return this.object;
	}

	/* -------------------------------------------- */

	/**
	 * The BaseEntitySheet requires that the form itself be editable as well as the entity be owned
	 * @type {Boolean}
	 */
	get isEditable() {
		return this.options.editable && this.entity.owner;
	}

	/* -------------------------------------------- */

	/**
	 * Assign the default options which are supported by the entity edit sheet
	 */
	static get defaultOptions() {
		const options = super.defaultOptions;
		options.classes = ["sheet"];
		options.template = `templates/sheets/${this.name.toLowerCase()}.html`;
		return options;
	}

	/* -------------------------------------------- */

	/**
	 * The displayed window title for the sheet - the entity name by default
	 * @type {String}
	 */
	get title() {
		return this.entity.name;
	}

	/* -------------------------------------------- */

	/**
	 * Default data preparation logic for the entity sheet
	 */
	getData() {
		let isOwner = this.entity.owner;
		return {
			entity: duplicate(this.entity.data),
			owner: isOwner,
			limited: this.entity.limited,
			options: this.options,
			editable: this.isEditable,
			cssClass: isOwner ? "editable" : "locked"
		}
	}

	/* -------------------------------------------- */

	/**
	 * Extend the definition of header buttons for Entity Sheet forms to include an option to import from a Compendium
	 * @private
	 */
	_getHeaderButtons() {
		const buttons = super._getHeaderButtons();
		if ( this.options.compendium ) {
			buttons.unshift({
				label: "Import",
				class: "import",
				icon: "fas fa-download",
				onclick: async ev => {
					await this.close();
					this.entity.collection.importFromCollection(this.options.compendium, this.entity._id);
				}
			});
		}
		return buttons;
	}

	/* -------------------------------------------- */

	/**
	 * Implement the _updateObject method as required by the parent class spec
	 * This defines how to update the subject of the form when the form is submitted
	 * @private
	 */
	_updateObject(event, formData) {
		formData["_id"] = this.object._id;
		this.entity.update(formData);
	}
}
