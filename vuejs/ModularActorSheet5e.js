/**
 * Extend the basic ActorSheet class to do all the D&D5e things!
 * This sheet is an Abstract layer which is not used.
 */
class ModularActorSheet5e extends ModularActorSheet {

	constructor(actor, renderer, options) {
		super(actor, renderer, options);

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * replaces activateListeners(). Made idempotent.
		 */
		this.renderer.addOnPostRender((html) => {
			// Pad field width
			html.find('[data-wpad]')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('[data-wpad]' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('[data-wpad]' + '_idempotent_marker', true);
					return true;
				}).each((i, e) => {
				let text = e.tagName === "INPUT" ? e.value : e.innerText,
					w = text.length * parseInt(e.getAttribute("data-wpad")) / 2;
				e.setAttribute("style", "flex: 0 0 " + w + "px");
			});

			// Activate tabs
			html.find('.tabs')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.tabs' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.tabs' + '_idempotent_marker', true);
					return true;
				}).each((_, el) => {
				let tabs = $(el),
					group = el.getAttribute("data-group"),
					initial = this.actor.data.flags[`_sheetTab-${group}`];
				new Tabs(tabs, {
					initial: initial,
					callback: clicked => this.actor.data.flags[`_sheetTab-${group}`] = clicked.attr("data-tab")
				});
			});

			// Item summaries
			html.find('.item .item-name h4')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item .item-name h4' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item .item-name h4' + '_idempotent_marker', true);
					return true;
				}).click(event => this._onItemSummary(event));

			// Everything below here is only needed if the sheet is editable
			if (!this.options.editable) return;

			/* -------------------------------------------- */
			/*  Abilities, Skills, and Traits
			 /* -------------------------------------------- */

			// Ability Proficiency
			html.find('.ability-proficiency')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.ability-proficiency' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.ability-proficiency' + '_idempotent_marker', true);
					return true;
				})
				.click(ev => {
					let field = $(ev.currentTarget).siblings('input[type="hidden"]');
					this.actor.update({[field[0].name]: 1 - parseInt(field[0].value)});
				});

			// Ability Checks
			html.find('.ability-name')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.ability-name' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.ability-name' + '_idempotent_marker', true);
					return true;
				}).click(event => {
				event.preventDefault();
				let ability = event.currentTarget.parentElement.getAttribute("data-ability");
				this.actor.rollAbility(ability, {event: event});
			});

			// Toggle Skill Proficiency
			html.find('.skill-proficiency')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.skill-proficiency' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.skill-proficiency' + '_idempotent_marker', true);
					return true;
				}).on("click contextmenu", this._onCycleSkillProficiency.bind(this));

			// Roll Skill Checks
			html.find('.skill-name')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.skill-name' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.skill-name' + '_idempotent_marker', true);
					return true;
				}).click(ev => {
				let skl = ev.currentTarget.parentElement.getAttribute("data-skill");
				this.actor.rollSkill(ev, skl);
			});

			// Trait Selector
			html.find('.trait-selector')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.trait-selector' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.trait-selector' + '_idempotent_marker', true);
					return true;
				}).click(ev => this._onTraitSelector(ev));

			// Configure Special Flags
			html.find('.configure-flags')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.configure-flags' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.configure-flags' + '_idempotent_marker', true);
					return true;
				}).click(this._onConfigureFlags.bind(this));

			/* -------------------------------------------- */
			/*  Inventory
			/* -------------------------------------------- */

			// Create New Item
			html.find('.item-create')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item-create' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item-create' + '_idempotent_marker', true);
					return true;
				}).click(ev => this._onItemCreate(ev));

			// Update Inventory Item
			html.find('.item-edit')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item-edit' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item-edit' + '_idempotent_marker', true);
					return true;
				}).click(ev => {
				let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id"));
				const item = this.actor.getOwnedItem(itemId);
				item.sheet.render(true);
			});

			// Delete Inventory Item
			html.find('.item-delete')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item-delete' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item-delete' + '_idempotent_marker', true);
					return true;
				}).click(ev => {
				let li = $(ev.currentTarget).parents(".item"),
					itemId = Number(li.attr("data-item-id"));
				this.actor.deleteOwnedItem(itemId);
				li.slideUp(200, () => this.render(false));
			});

			// Toggle Spell prepared value
			html.find('.item-prepare')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item-prepare' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item-prepare' + '_idempotent_marker', true);
					return true;
				}).click(ev => {
				let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id")),
					item = this.actor.getOwnedItem(itemId);
				item.data['prepared'].value = !item.data['prepared'].value;
				this.actor.updateOwnedItem(item);
			});

			// Item Dragging
			let handler = ev => this._onDragItemStart(ev);
			html.find('.item')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item' + '_idempotent_marker', true);
					return true;
				}).each((i, li) => {
				li.setAttribute("draggable", true);
				li.addEventListener("dragstart", handler, false);
			});

			// Item Rolling
			html.find('.item .item-image')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.item .item-image' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.item .item-image' + '_idempotent_marker', true);
					return true;
				}).click(event => this._onItemRoll(event));

			// Re-render the sheet when toggling visibility of spells
			html.find('.prepared-toggle')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.prepared-toggle' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.prepared-toggle' + '_idempotent_marker', true);
					return true;
				}).click(ev => {
				this.options.showUnpreparedSpells = !this.options.showUnpreparedSpells;
				this.render()
			});
		});
		/**
		 * END NEW_MODULAR_RENDERER
		 */
	}

	/**
	 * Return the type of the current Actor
	 * @type {String}
	 */
	get actorType() {
		return this.actor.data.type;
	}

	/* -------------------------------------------- */

	/**
	 * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
	 */
	getData() {
		const sheetData = super.getData();

		// Ability proficiency
		for (let abl of Object.values(sheetData.data.abilities)) {
			abl.icon = this._getProficiencyIcon(abl.proficient);
			abl.hover = CONFIG.proficiencyLevels[abl.proficient];
		}

		// Update skill labels
		for (let skl of Object.values(sheetData.data.skills)) {
			skl.ability = sheetData.data.abilities[skl.ability].label.substring(0, 3);
			skl.icon = this._getProficiencyIcon(skl.value);
			skl.hover = CONFIG.proficiencyLevels[skl.value];
		}

		// Update traits
		sheetData["actorSizes"] = CONFIG.actorSizes;
		this._prepareTraits(sheetData.data["traits"]);

		// Prepare owned items
		this._prepareItems(sheetData);

		// Return data to the sheet
		return sheetData;
	}

	/* -------------------------------------------- */

	_prepareTraits(traits) {
		const map = {
			"dr": CONFIG.damageTypes,
			"di": CONFIG.damageTypes,
			"dv": CONFIG.damageTypes,
			"ci": CONFIG.conditionTypes,
			"languages": CONFIG.languages
		};
		for (let [t, choices] of Object.entries(map)) {
			const trait = traits[t];
			trait.selected = trait.value.reduce((obj, t) => {
				obj[t] = choices[t];
				return obj;
			}, {});

			// Add custom entry
			if (trait.custom) trait.selected["custom"] = trait.custom;
		}
	}

	/* -------------------------------------------- */

	/**
	 * Insert a spell into the spellbook object when rendering the character sheet
	 * @param {Object} actorData    The Actor data being prepared
	 * @param {Object} spellbook    The spellbook data being prepared
	 * @param {Object} spell        The spell data being prepared
	 * @private
	 */
	_prepareSpell(actorData, spellbook, spell) {
		let lvl = spell.data.level.value || 0,
			isNPC = this.actorType === "npc";

		// Determine whether to show the spell
		let showSpell = this.options.showUnpreparedSpells || isNPC || spell.data.prepared.value || (lvl === 0);
		if (!showSpell) return;

		// Extend the Spellbook level
		spellbook[lvl] = spellbook[lvl] || {
			isCantrip: lvl === 0,
			label: CONFIG.spellLevels[lvl],
			spells: [],
			uses: actorData.data.spells["spell" + lvl].value || 0,
			slots: actorData.data.spells["spell" + lvl].max || 0
		};

		// Add the spell to the spellbook at the appropriate level
		spell.data.school.str = CONFIG.spellSchools[spell.data.school.value];
		spellbook[lvl].spells.push(spell);
	}

	/* -------------------------------------------- */

	/**
	 * Get the font-awesome icon used to display a certain level of skill proficiency
	 * @private
	 */
	_getProficiencyIcon(level) {
		const icons = {
			0: '<i class="far fa-circle"></i>',
			0.5: '<i class="fas fa-adjust"></i>',
			1: '<i class="fas fa-check"></i>',
			2: '<i class="fas fa-check-double"></i>'
		};
		return icons[level];
	}


	/* -------------------------------------------- */
	/*  Event Listeners and Handlers                */

	/* -------------------------------------------- */

	/**
	 * Handle click events for the Traits tab button to configure special Character Flags
	 */
	_onConfigureFlags(event) {
		event.preventDefault();
		new ActorSheetFlags(this.actor).render(true);
	}

	/* -------------------------------------------- */

	/**
	 * Handle cycling proficiency in a Skill
	 * @param {Event} event   A click or contextmenu event which triggered the handler
	 * @private
	 */
	_onCycleSkillProficiency(event) {
		event.preventDefault();
		const field = $(event.currentTarget).siblings('input[type="hidden"]');

		// Get the current level and the array of levels
		const level = parseFloat(field.val());
		const levels = [0, 1, 0.5, 2];
		let idx = levels.indexOf(level);

		// Toggle next level - forward on click, backwards on right
		if (event.type === "click") {
			field.val(levels[(idx === levels.length - 1) ? 0 : idx + 1]);
		} else if (event.type === "contextmenu") {
			field.val(levels[(idx === 0) ? levels.length - 1 : idx - 1]);
		}

		// Update the field value and save the form
		this._onSubmit(event);
	}

	/* -------------------------------------------- */

	/**
	 * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
	 * @private
	 */
	_onItemRoll(event) {
		event.preventDefault();
		let itemId = Number($(event.currentTarget).parents(".item").attr("data-item-id")),
			item = this.actor.getOwnedItem(itemId);
		item.roll();
	}

	/* -------------------------------------------- */

	/**
	 * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
	 * @private
	 */
	_onItemSummary(event) {
		event.preventDefault();
		let li = $(event.currentTarget).parents(".item"),
			item = this.actor.getOwnedItem(Number(li.attr("data-item-id"))),
			chatData = item.getChatData({secrets: this.actor.owner});

		// Toggle summary
		if (li.hasClass("expanded")) {
			let summary = li.children(".item-summary");
			summary.slideUp(200, () => summary.remove());
		} else {
			let div = $(`<div class="item-summary">${chatData.description.value}</div>`);
			let props = $(`<div class="item-properties"></div>`);
			chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
			div.append(props);
			li.append(div.hide());
			div.slideDown(200);
		}
		li.toggleClass("expanded");
	}


	/* -------------------------------------------- */

	/**
	 * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
	 * @private
	 */
	_onItemCreate(event) {
		event.preventDefault();
		let header = event.currentTarget,
			data = duplicate(header.dataset);
		data["name"] = `New ${data.type.capitalize()}`;
		this.actor.createOwnedItem(data, {renderSheet: true});
	}

	/* -------------------------------------------- */

	_onTraitSelector(event) {
		event.preventDefault();
		let a = $(event.currentTarget);
		const options = {
			name: a.parents("label").attr("for"),
			title: a.parent().text().trim(),
			choices: CONFIG[a.attr("data-options")]
		};
		new TraitSelector5e(this.actor, options).render(true)
	}
}
