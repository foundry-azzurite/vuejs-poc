'use strict';

/**
 * NEW_MODULAR_RENDERER
 *
 * basically all .html handlebar template files in dnd5e/templates/actors/ added as strings here
 * and converted into Vue.js templates instead of handlebar templates
 */
(() => {

	const biography = `
<div class="tab biography" data-group="primary" data-tab="biography">
    <tinymce-editor 
		:content="data.details.biography.value" 
		:target="'data.details.biography.value'" 
		showButton="true" 
		:isOwner="owner"
		:isEditable="editable" />
</div>
`;

	const traits = `
<div class="form-group trait-size">
    <label>{{data.traits.size.label}}</label>
    <select v-model="data.traits.size.value" name="data.traits.size.value" v-bind:data-dtype="data.traits.size.type">
		<option v-for= "(label, size) in actorSizes" v-bind:value="size">{{label}}</option>
    </select>
</div>

<div class="form-group trait-perception">
    <label>{{data.traits.perception.label}}</label>
    <input type="text" name="data.traits.perception.value" v-bind:data-dtype="data.traits.perception.type"
           v-model="data.traits.perception.value" placeholder="Value"/>
</div>

<div class="form-group-stacked">
    <label>{{data.traits.senses.label}}</label>
    <input type="text" name="data.traits.senses.value" v-bind:data-dtype="data.traits.senses.type"
           v-model="data.traits.senses.value" placeholder="None"/>
</div>

<div class="form-group-stacked">
    <label for="data.traits.languages">
        {{data.traits.languages.label}}
        <a class="trait-selector" data-options="languages"><i class="fas fa-edit"></i></a>
    </label>
    <ul class="traits-list">
        <li v-for="(v, k) in data.traits.languages.selected" class="tag" v-bind:class="k">{{v}}</li>
    </ul>
</div>

<div class="form-group-stacked">
    <label for="data.traits.di">
        {{data.traits.di.label}}
        <a class="trait-selector" data-options="damageTypes"><i class="fas fa-edit"></i></a>
    </label>
    <ul class="traits-list">
		<li v-for="(v, k) in data.traits.di.selected" class="tag" v-bind:class="k">{{v}}</li>
    </ul>
</div>

<div class="form-group-stacked">
    <label for="data.traits.dr">
        {{data.traits.dr.label}}
        <a class="trait-selector" data-options="damageTypes"><i class="fas fa-edit"></i></a>
    </label>
    <ul class="traits-list">
        <li v-for="(v, k) in data.traits.dr.selected" class="tag" v-bind:class="k">{{v}}</li>
    </ul>
</div>

<div class="form-group-stacked">
    <label for="data.traits.dv">
        {{data.traits.dv.label}}
        <a class="trait-selector" data-options="damageTypes"><i class="fas fa-edit"></i></a>
    </label>
    <ul class="traits-list">
    	<li v-for="(v, k) in data.traits.dv.selected" class="tag" v-bind:class="k">{{v}}</li>
    </ul>
</div>

<div class="form-group-stacked">
    <label for="data.traits.ci">
        {{data.traits.ci.label}}
        <a class="trait-selector" data-options="conditionTypes"><i class="fas fa-edit"></i></a>
    </label>
    <ul class="traits-list">
        <li v-for="(v, k) in data.traits.ci.selected" class="tag" v-bind:class="k">{{v}}</li>
    </ul>
</div>

<div class="form-group configure-flags">
    <label>Special Traits</label>
    <i class="fas fa-cog"></i>
</div>

`;

	const skills = `
<ol class="skills-list">
    <li v-for="(skill, s) in data.skills" class="skill" v-bind:data-skill="s">
        <input type="hidden" v-bind:name="'data.skills.' + s + '.value'" v-model="skill.value" data-dtype="Number"/>
        <a class="skill-proficiency" v-bind:title="skill.hover" v-html="skill.icon"></a>
        <h4 class="skill-name rollable">{{skill.label}}</h4>
        <span class="skill-ability">{{skill.ability}}</span>
        <span class="skill-mod">{{$numberFormat(skill.mod, 0, true)}}</span>
    </li>
</ol>
`;
	const classes = `
<section class="class-levels">
    <header class="class-header">
        <h3>Class Levels</h3>
        <a v-if="owner" class="item-create" data-type="class" data-levels="1"><i class="fa fa-plus"></i> Add</a>
    </header>

    <ol class="class-list inventory-list directory-list">
        <li v-for="cls in actor.classes" class="item" v-bind:data-item-id="cls.id">
            <header class="item-header">
                <h4 class="class-name">{{cls.name}}</h4>
                <span class="class-levels">{{cls.data.levels.value}}</span>
            </header>
            <div class="class-subclass">
                <h5>{{cls.data.subclass.value}}</h5>
                <div v-if="owner" class="item-controls">
                    <a class="item-control item-edit" title="Edit Item"><i class="fas fa-edit"></i></a>
                    <a class="item-control item-delete" title="Delete Item"><i class="fas fa-trash"></i></a>
                </div>
            </div>
        </li>
    </ol>
</section>
`;

	const abilities = `
<ol class="abilities-list">
    <li v-for="(ability, id) in data.abilities" class="ability" v-bind:data-ability="id">
        <input type="hidden" v-bind:name="'data.abilities.' + id + '.proficient'" v-model="ability.proficient"
               data-dtype="Number"/>
        <a class="ability-proficiency" v-bind:title="ability.hover" v-html="ability.icon"></a>
        <h4 class="ability-name rollable">{{ability.label}}</h4>
        <span class="ability-mod" title="Modifier">{{$numberFormat(ability.mod, 0, true)}}</span>
        <span class="ability-score" title="Ability Score">
            <input v-bind:name="'data.abilities.' + id + '.value'" type="text" v-model="ability.value"
                   data-dtype="Number" placeholder="10"/>
        </span>
        <span class="ability-save" title="Saving Throw">{{$numberFormat(ability.save, 0, true)}}</span>
    </li>
</ol>
	`;
const attributes = `
<div class="primary-attributes">
    <div class="attribute">
        <h4 class="attribute-name">Health</h4>
        <div class="attribute-value flexrow">
            <input name="data.attributes.hp.value" type="text" v-model="data.attributes.hp.value"
                   v-bind:data-dtype="data.attributes.hp.type" placeholder="10"/>
            <span class="flex0"> / </span>
            <input name="data.attributes.hp.max" type="text" v-model="data.attributes.hp.max"
                   v-bind:data-dtype="data.attributes.hp.type" placeholder="10"/>
        </div>
        <footer class="attribute-footer">
            <input name="data.attributes.hp.temp" type="text" class="temphp" placeholder="+Temp"
                   v-model="data.attributes.hp.temp" v-bind:data-dtype="data.attributes.hp.type"/>
            <input name="data.attributes.hp.tempmax" type="text" class="temphp" placeholder="+Max"
                   v-model="data.attributes.hp.tempmax" v-bind:data-dtype="data.attributes.hp.type"/>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">Armor Class</h4>
        <div class="attribute-value flexrow">
            <input name="data.attributes.ac.value" type="text" v-model="data.attributes.ac.value"
                   v-bind:data-dtype="data.attributes.ac.type" placeholder="10"/>
        </div>
        <footer class="attribute-footer">
            <span>Base AC {{data.attributes.ac.min}}</span>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">Proficiency</h4>
        <div class="attribute-value flexrow">
            <span>{{$numberFormat(data.attributes.prof.value, 0, true)}}</span>
        </div>
        <footer class="attribute-footer">
            <span>Spell DC {{data.attributes.spelldc.value}}</span>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">Initiative</h4>
        <div class="attribute-value flexrow">
            <span>{{$numberFormat(data.attributes.init.total, 0, true)}}</span>
        </div>
        <footer class="attribute-footer">
            <span>Modifier</span>
            <input name="data.attributes.init.value" type="text" placeholder="0"
                   v-bind:data-dtype="data.attributes.init.type"
                   v-bind:value="$numberFormat(data.attributes.init.value, 0, true)"/>
        </footer>
    </div>
</div>

<div class="primary-attributes">
    <div class="attribute">
        <h4 class="attribute-name">Hit Dice</h4>
        <div class="attribute-value flexrow">
            <input name="data.attributes.hd.value" type="text" placeholder="1"
                   v-model="data.attributes.hd.value" v-bind:data-dtype="data.attributes.hd.type"/>
            <span class="flex0"> / </span>
            <span>{{data.details.level.value}}</span>
        </div>
        <footer class="attribute-footer">
            <a class="rest short-rest">S. Rest</a>
            <a class="rest long-rest">L. Rest</a>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">
            <input name="data.resources.primary.label" type="text" class="resource-label" placeholder="Primary"
                   v-model="data.resources.primary.label" data-dtype="String" />
        </h4>
       <div class="attribute-value flexrow">
           <input name="data.resources.primary.value" type="text" v-model="data.resources.primary.value"
                  data-dtype="Number" placeholder="0"/>
           <span class="flex0"> / </span>
           <input name="data.resources.primary.max" type="text" v-model="data.resources.primary.max"
                  data-dtype="Number" placeholder="0"/>
       </div>
        <footer class="attribute-footer flexrow recharge">
            <span>SR</span>
            <input name="data.resources.primary.sr" type="checkbox" data-dtype="Boolean"
                   v-model="data.resources.primary.sr" />
            <span>LR</span>
            <input name="data.resources.primary.lr" type="checkbox" data-dtype="Boolean"
                   v-model="data.resources.primary.lr"/>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">
            <input name="data.resources.secondary.label" type="text" class="resource-label"
                   placeholder="Secondary" v-model="data.resources.secondary.label" data-dtype="String"/>
        </h4>
       <div class="attribute-value flexrow">
           <input name="data.resources.secondary.value" type="text" v-model="data.resources.secondary.value"
                  data-dtype="Number" placeholder="0"/>
           <span class="flex0"> / </span>
           <input name="data.resources.secondary.max" type="text" v-model="data.resources.secondary.max"
                  data-dtype="Number" placeholder="0"/>
       </div>
        <footer class="attribute-footer flexrow recharge">
            <span>SR</span>
            <input name="data.resources.secondary.sr" type="checkbox" data-dtype="Boolean"
                   v-model="data.resources.secondary.sr"/>
            <span>LR</span>
            <input name="data.resources.secondary.lr" type="checkbox" data-dtype="Boolean"
                   v-model="data.resources.secondary.lr"/>
        </footer>
    </div>

    <div class="attribute">
        <h4 class="attribute-name">Speed</h4>
        <div class="attribute-value flexrow">
            <span class="flex0">  </span>
            <input name="data.attributes.speed.value" type="text" v-model="data.attributes.speed.value"
                   v-bind:data-dtype="data.attributes.speed.type" placeholder="0"/>
        </div>
        <footer class="attribute-footer">
            <input type="text" class="speed" name="data.attributes.speed.special" placeholder="Swim / Fly"
                   v-bind:data-dtype="data.attributes.speed.type" v-model="data.attributes.speed.special"/>
        </footer>
    </div>
</div>
`;

window.testTemplate = `
<form v-bind:class="cssClass" autocomplete="off">

    <!-- UPPER SECTION -->
    <section class="sheet-upper">

        <!-- HEADER -->
        <header class="sheet-header">
            <h1 class="charname">
                <input name="name" type="text" v-model="actor.name" placeholder="Character Name"/>
            </h1>

            <div class="charlevel">
                <div class="level" v-bind:class="{noxp: disableExperience}">
                    <label>Level </label>
                    <input name="data.details.level.value" type="text" v-model="data.details.level.value"
                           data-dtype="Number" placeholder="1"/>
                </div>
                <template v-if="disableExperience">
                </template>
				<template v-else>
                    <div class="xpbar">
                        <span class="bar" v-bind:style="{width: data.details.xp.pct + '%'}"></span>
                    </div>
                    <div class="experience">
                        <input name="data.details.xp.value" type="text" v-model="data.details.xp.value"
                               data-dtype="Number" placeholder="0"/>
                        <span class="max" data-wpad="14"> / {{data.details.xp.max}}</span>
                    </div>
				</template>
            </div>
        </header>

        <!-- PROFILE ARTWORK -->
        <img class="sheet-profile sidebar" v-bind:src="actor.img" v-bind:title="actor.name" height="220" width="220" data-edit="img"/>

        <!-- SHOWCASE -->
        <section class="sheet-showcase content">

            <!-- DETAILS -->
            <ul class="details">
                <li class="character-detail">
                    <input name="data.details.race.value" type="text" v-model="data.details.race.value" placeholder="Race"/>
                </li>
                <li class="character-detail">
                    <input name="data.details.background.value" type="text" v-model="data.details.background.value" placeholder="Background"/>
                </li>
                <li class="character-detail">
                    <input name="data.details.alignment.value" type="text" v-model="data.details.alignment.value" placeholder="Alignment"/>
                </li>
            </ul>

            <!-- ATTRIBUTES -->
            ${attributes}

            <!-- STATUS EFFECTS -->
            <div class="statuses">
                <div class="status death">
                    <h4>Death Saves</h4>
                    <div class="status-value">
                        <i class="fas fa-check"></i>
                        <input type="text" name="data.attributes.death.success" data-dtype="Number" placeholder="0"
                               v-model="data.attributes.death.success"/>
                        <i class="fas fa-times"></i>
                        <input type="text" name="data.attributes.death.failure" data-dtype="Number" placeholder="0"
                               v-model="data.attributes.death.failure"/>
                    </div>
                </div>
                <div class="status exhaustion">
                    <h4>Exhaustion</h4>
                    <div class="status-value">
                        <input type="text" name="data.attributes.exhaustion.value" data-dtype="Number" placeholder="0"
                               v-model="data.attributes.exhaustion.value" />
                    </div>
                </div>
                <div class="status inspiration">
                    <h4>Inspiration</h4>
                    <div class="status-value">
                        <input type="checkbox" name="data.attributes.inspiration.value" data-dtype="Boolean"
                               v-model=" data.attributes.inspiration.value" />
                    </div>
                </div>
            </div>
        </section>
    </section>

    <!-- SHEET NAVIGATION -->
    <nav class="sheet-navigation">
        <nav class="sheet-tabs tabs sidebar" data-group="sidebar">
            <a class="item active" data-tab="abilities">Abilities</a>
            <a class="item" data-tab="skills">Skills</a>
            <a class="item" data-tab="traits">Traits</a>
        </nav>
        <nav class="sheet-tabs tabs content" data-group="primary">
            <a class="item" data-tab="inventory">Inventory</a>
            <a class="item" data-tab="spellbook">Spellbook</a>
            <a class="item" data-tab="feats">Feats</a>
            <a class="item active" data-tab="biography">Biography</a>
        </nav>
    </nav>

    <!-- LOWER SECTION -->
    <section class="sheet-lower">

        <!-- SIDEBAR -->
        <section class="sheet-sidebar sidebar">

            <!-- ABILITY SCORES -->
            <div class="tab abilities" data-group="sidebar" data-tab="abilities">
                ${abilities}
                ${classes}
            </div>

            <!-- SKILLS -->
            <div class="tab skills" data-group="sidebar" data-tab="skills">
                ${skills}
            </div>

            <!-- TRAITS -->
            <div class="tab traits" data-group="sidebar" data-tab="traits">
                ${traits}
            </div>
        </section>

        <!-- BODY -->
        <section class="sheet-content content">

            <!-- INVENTORY -->
            <div class="tab inventory" data-group="primary" data-tab="inventory">
                <ol class="currency">
                    <li v-for="(c, i) in data.currency" class="denomination" :class="i">
                        <label>{{c.label}}:</label>
                        <input type="text" v-bind:name="'data.currency.' + i + '.value'" v-model="c.value" v-bind:data-dtype="c.type"/>
                    </li>
                </ol>

                <ol class="inventory-list directory-list">
                <template v-for="(section, sid) in actor.inventory">
                    <li  class="item flexrow inventory-header">
                        <h3 class="item-name flexrow">{{section.label}}</h3>
                        <span class="item-quantity">Qty.</span>
                        <span class="item-weight">Wt.</span>
                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-create" title="Create Item" :data-type="sid"><i class="fas fa-plus"></i> Add</a>
                        </div>
                    </li>
                    <li v-for="(item, iid) in section.items" class="item flexrow" :data-item-id="item.id">
                        <div class="item-name flexrow rollable">
                            <div class="item-image" :style="{backgroundImage: 'url(' + item.img + ')'}"></div>
                            <h4>
                                {{item.name}}
                                <i v-if="item.data.attuned.value" class="prepared fas fa-haykal"></i>
                            </h4>
                            <span v-if="item.hasCharges" class="item-charges">({{item.data.charges.value}}/{{item.data.charges.max}})</span>
                        </div>
                        <span class="item-quantity">{{item.data.quantity.value}}</span>
                        <span class="item-weight">{{item.totalWeight}}</span>

                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-edit" title="Edit Item"><i class="fas fa-edit"></i></a>
                            <a class="item-control item-delete" title="Delete Item"><i class="fas fa-trash"></i></a>
                        </div>
                    </li>
				</template>
                </ol>

                <div class="encumbrance" :class="{encumbered: data.attributes.encumbrance.encumbered}" >
                    <span class="encumbrance-bar" :style="{width: data.attributes.encumbrance.pct + '%'}"></span>
                    <span class="encumbrance-label">{{data.attributes.encumbrance.value}} / {{data.attributes.encumbrance.max}}</span>
                    <div class="encumbrance-breakpoint arrow-up"></div>
                    <div class="encumbrance-breakpoint arrow-down"></div>
                </div>
            </div>

            <!-- SPELLBOOK -->
            <div class="tab spellbook" data-group="primary" data-tab="spellbook">
                <div class="form-group spellcasting-ability">
                    <label>Spellcasting Ability</label>
                    <select v-model="data.attributes.spellcasting.value" name="data.attributes.spellcasting.value" data-type="String">
                        <option value="">None</option>
                        <option v-for="(abl, a) in data.abilities" :value="a">{{abl.label}}</option>
                    </select>
                    <a class="prepared-toggle" title="Toggle Spell Visibility"><i class="fas fa-haykal"></i></a>
                </div>

                <ol class="inventory-list directory-list">
                <template v-for="(section, lvl) in actor.spellbook">
                    <li  class="item flexrow inventory-header spellbook-header">
                        <div class="item-name flexrow">
                            <h3>{{section.label}}</h3>

							<template v-if="section.isCantrip">
								<span class="spell-slots">&infin;</span>
								<span class="flex0"> / </span>
								<span c	lass="spell-max">&infin;</span>
                            </template>
                            <template v-else>
								<span class="spell-slots">
									<input type="text" :name="'data.spells.spell' + lvl + '.value'" v-model="section.uses" placeholder="0"/>
								</span>
								<span class="flex0"> / </span>
								<span class="spell-max">
									<input type="text" :name="'data.spells.spell' + lvl + '.max'" v-model="section.slots" placeholder="0"/>
								</span>
                            </template>
                        </div>

                        <div class="spell-school">School</div>
                        <div class="spell-action">Action</div>

                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-create" title="Create Spell" data-type="spell"
                               :data-level="lvl"><i class="fas fa-plus"></i> Add</a>
                        </div>
                    </li>

                    <li v-for="(item, i) in section.spells" class="item flexrow" :data-item-id="item.id">
                        <div class="item-name flexrow rollable">
                            <div class="item-image" :style="{backgroundImage: 'url(' + item.img + ')'}"></div>
                            <h4>{{item.name}}</h4>
                        </div>

                        <div class="spell-school">{{item.data.school.str}}</div>
                        <div class="spell-action">{{item.data.time.value}}</div>

                        <div v-if="owner" class="item-controls" :style="[section.isCantrip ? {paddingLeft: '22px'} : {}]">
                            <template v-if="!section.isCantrip">
								<a v-if="item.data.prepared.value" class="item-control item-prepare" title="unprepare Spell"><i class="fas fa-haykal"></i></a>
								<a v-else class="item-control item-prepare" title="prepare Spell"><i class="fas fa-circle"></i></a>
                            </template>
                            <a class="item-control item-edit" title="Edit Item"><i class="fas fa-edit"></i></a>
                            <a class="item-control item-delete" title="Delete Item"><i class="fas fa-trash"></i></a>
                        </div>
                    </li>
                </template>
                    <li v-if="!actor.spellbook" class="item flexrow inventory-header spellbook-header spellbook-empty">
                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-create" title="Create Spell" data-type="spell"
                               :data-level="lvl"><i class="fas fa-plus"></i> Add Spell</a>
                        </div>
                    </li>
                </ol>
           </div>

            <!-- FEATS -->
            <div class="tab feats" data-group="primary" data-tab="feats">
                <ol class="feats-list inventory-list directory-list">
                    <li class="item flexrow inventory-header">
                        <div class="item-name flexrow">
                            <h3>Ability Name</h3>
                        </div>
                        <div class="ability-req">Requirements</div>

                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-create" title="Create Ability" data-type="feat" data-featType="passive">
                                <i class="fas fa-plus"></i> Add</a>
                        </div>
                    </li>

                    <li v-for="(item, i) in actor.feats" class="item flexrow" :data-item-id="item.id">
                        <div class="item-name flexrow rollable">
                            <div class="item-image" :style="{backgroundImage: 'url(' + item.img + ')'}"></div>
                            <h4>{{item.name}} <span v-if="item.data.uses.max" class="feat-uses">({{item.data.uses.value}} / {{item.data.uses.max}})</span></h4>
                        </div>

                        <div class="ability-req">{{item.data.requirements.value}}</div>

                        <div v-if="owner" class="item-controls">
                            <a class="item-control item-edit" title="Edit Ability"><i class="fas fa-edit"></i></a>
                            <a class="item-control item-delete" title="Delete Ability"><i class="fas fa-trash"></i></a>
                        </div>
                    </li>
                </ol>
            </div>

            <!-- BIOGRAPHY -->
            ${biography}
        </section>
    </section>
</form>
`;

})();
