class ModularApplication {
	constructor(renderer, options) {

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * handle new renderer constructor arg
		 */
		if (options === undefined && !(renderer instanceof ApplicationRenderer)) {
			console.warn(`Providing no renderer in the Application constructor is deprecated!`);
			options = renderer;
			renderer = undefined;
		}
		// END NEW_MODULAR_RENDERER

		/**
		 * The options provided to this application upon initialization
		 * @type {Object}
		 */
		this.options = mergeObject(this.constructor.defaultOptions, options || {}, {
			insertKeys: true,
			insertValues: false,
			overwrite: true,
			inplace: false
		});


		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * handle all deprecation stuff
		 */
		if (renderer === undefined) {
			this.options.template = this.options.template || this.template;
			renderer = new HandlebarsRenderer(this.options);
		}

		this.renderer = renderer;
		if (this.activateListeners !== ModularApplication.prototype.activateListeners) {
			console.warn(`Overriding activateListeners is deprecated. Provide your method to HandlebarsRenderer.addOnPostRender instead.`);
			renderer.addOnPostRender(this.activateListeners.bind(this));
		}

		if (this._renderInner !== ModularApplication.prototype._renderInner) {
			console.warn(`Overriding _renderInner is deprecated. Add a HandlebarsRenderer with overridden HandlebarsRenderer.render instead.`);
		}

		if (this._getHeaderButtons !== ModularApplication.prototype._getHeaderButtons) {
			console.warn(`Overriding _getHeaderButtons is deprecated. Instead, add your headerButtons to the .headerButtons array property.`);
		}

		const templateDescriptor = Object.getOwnPropertyDescriptor(this.constructor.prototype, `template`);
		if (templateDescriptor && templateDescriptor.get !== Object.getOwnPropertyDescriptor(ModularApplication.prototype, `template`).get) {
			console.warn(`Overriding get template() is deprecated. Provide the template to the options instead.`);
			this.options.template = this.template;
			this.renderer.options.template = this.template;
		}
		// END NEW_MODULAR_RENDERER

		/**
		 * The application ID is a unique incrementing integer which is used to identify every application window
		 * drawn by the VTT
		 * @type {Number}
		 */
		this.appId = _appId += 1;

		/**
		 * Track the current position and dimensions of the Application UI
		 * @type {Object}
		 */
		this.position = {
			width: this.options.width,
			height: this.options.height,
			left: this.options.left,
			top: this.options.top,
			scale: this.options.scale
		};

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * provide an array of headerButtons instead of overriding a method. Not strictly necessary for
		 * the NEW_MODULAR_RENDERER refactoring, but I did it anyway... idk why
		 */
		this.headerButtons = [
			{
				label: "Close",
				class: "close",
				icon: "fas fa-times",
				onclick: ev => this.close()
			}
		];
		// END NEW_MODULAR_RENDERER

		/**
		 * An internal reference to the HTML element this application renders
		 * @type {jQuery}
		 */
		this._element = null;

		/**
		 * Track whether the Application is currently minimized
		 * @type {Boolean}
		 * @private
		 */
		this._minimized = false;

		/**
		 * Track whether the Application has been successfully rendered
		 * @type {Boolean}
		 * @private
		 */
		this._rendered = false;
	}


	/**
	 * Assign the default options which are supported by this Application
	 */
	static get defaultOptions() {
		let config = CONFIG[this.name] || {};
		return {
			width: config.width,
			height: config.height,
			top: null,
			left: null,
			popOut: true,
			minimizable: true,
			id: "",
			classes: [],
			title: ""
		};
	};

	/**
	 * Return the CSS application ID which uniquely references this UI element
	 */
	get id() {
		return this.options.id ? this.options.id : `app-${this.appId}`;
	}

	/**
	 * Return the active application element, if it currently exists in the DOM
	 * @type {jQuery|HTMLElement}
	 */
	get element() {
		if (this._element) return this._element;
		let selector = "#" + this.id;
		return $(selector);
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * template should be on the options so the renderer gets the template
	 *
	 * @deprecated
	 */
	get template() {
		return this.options.template;
	}

	/**
	 * Control the rendering style of the application. If popOut is true, the application is rendered in its own
	 * wrapper window, otherwise only the inner app content is rendered
	 * @type {boolean}
	 */
	get popOut() {
		return (this.options.popOut !== undefined) ? Boolean(this.options.popOut) : true;
	}

	/**
	 * An Application window should define its own title definition logic which may be dynamic depending on its data
	 * @type {String}
	 */
	get title() {
		return this.options.title;
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * this is now deprecated and will only be used if a subclass still overrides it
	 *
	 * @deprecated
	 */
	activateListeners(html) {}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * this is now deprecated and will only be used if a subclass still overrides it
	 *
	 * @deprecated
	 */
	_getHeaderButtons() {
		return [];
	}

	/**
	 * An application should define the data object used to render its template.
	 * This function may either return an Object directly, or a Promise which resolves to an Object
	 * If undefined, the default implementation will return an empty object allowing only for rendering of static HTML
	 *
	 * @return {Object|Promise}
	 */
	getData(options) {
		return {};
	}

	/**
	 * Render the Application by evaluating it's HTML template against the object of data provided by the getData
	 * method
	 * If the Application is rendered as a pop-out window, wrap the contained HTML in an outer frame with window
	 * controls
	 *
	 * @param {Object} data The data to render.
	 * @param {Boolean} force   Add the rendered application to the DOM if it is not already present. If false, the
	 *                          Application will only be re-rendered if it is already present.
	 * @param {Object} options  Additional rendering options which are applied to customize the way that the
	 *     Application
	 *                          is rendered in the DOM.
	 *
	 * @param {Number} options.left           The left positioning attribute
	 * @param {Number} options.top            The top positioning attribute
	 * @param {Number} options.width          The rendered width
	 * @param {Number} options.height         The rendered height
	 * @param {Number} options.scale          The rendered transformation scale
	 * @param {Boolean} options.log           Whether to display a log message that the Application was rendered
	 * @param {String} options.renderContext  A context-providing string which suggests what event triggered the render
	 * @param {*} options.renderData          The data change which motivated the render request
	 */
	render(force = false, options = {}) {
		this._render(force, options);
		return this;
	}


	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * (almost) completely adjusted
	 */
	async _render(force = false, options = {}) {
		this._rendered = false;

		// Obtain application data used for rendering
		const data = await this.getData(options);

		// Get the existing HTML element and data for rendering
		const isNew = !this.element.length;
		if (!force && isNew) return;
		if (options.log || (force && isNew)) console.log(`${vtt} | Rendering ${this.constructor.name}`);

		if (isNew) {
			this._element = await this._createNewElement(options);
		}

		const $toRenderOn = this.popOut ? this.element.find('.window-content').children() : this.element;

		// Render the inner content
		if (this._renderInner !== ModularApplication.prototype._renderInner) {
			const html = await this._renderInner(data, options);
			$toRenderOn.replaceWith(html);
			this.renderer.postRenderFuncs.forEach(func => func(html));
		} else {
			await this.renderer.render($toRenderOn, data, options);
		}

		if (isNew) {
			// NEW_MODULAR_RENDERER
			// Removed so the performance testing makes more sense
			// this._element.fadeIn(200);
			this._element.show();
		}

		// Set the application position (if it's not currently minimized)
		if (this.popOut && !this._minimized) this.setPosition(this.position);

		// Dispatch the render<Application> hook
		Hooks.call("render" + this.constructor.name, this, $toRenderOn, data);
		this._rendered = true;
	}

	/**
	 * NEW_MODULAR_RENDERER
	 */
	async _createNewElement(options) {
		const tempElem = document.createElement('app');
		let newElement;
		if (this.popOut) {
			const $outer = await this._renderOuter(options);
			ui.windows[this.appId] = this;
			$outer.find('.window-content').append(tempElem);
			newElement = $outer;
		} else {
			newElement = tempElem;
		}
		newElement.hide();
		$('body').append(newElement);
		return newElement;
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * this is now deprecated and will only be used if a subclass overrides this method still
	 * @deprecated
	 */
	async _renderInner(data, options) {
		let html = await renderTemplate(this.template, data);
		if (html === "") throw new Error(`No data was returned from template ${this.template}`);
		return $(html);
	}

	/* -------------------------------------------- */

	/**
	 * Render the outer application wrapper
	 * @return {Promise.<HTMLElement>}   A promise resolving to the constructed jQuery object
	 * @private
	 */
	async _renderOuter(options) {

		let headerButtons = this.headerButtons;
		if (this._getHeaderButtons !== ModularApplication.prototype._getHeaderButtons) {
			headerButtons = this._getHeaderButtons().concat(headerButtons);
		}
		const headerButtonsToShow = headerButtons.filter(btn => (btn.shouldAdd || (() => true))());

		// Gather basic application data
		const classes = options.classes || this.options.classes;
		const windowData = {
			id: this.id,
			classes: classes.join(" "),
			appId: this.appId,
			title: this.title,
			headerButtons: headerButtonsToShow
		};

		// Render the template and return the promise
		let html = await renderTemplate("templates/app-window.html", windowData);
		html = $(html);

		// Activate header button click listeners
		windowData.headerButtons.forEach(button => {
			const btn = html.find(`a.${button.class}`);
			btn.mousedown(ev => ev.stopPropagation()).mouseup(ev => {
				ev.preventDefault();
				button.onclick(ev);
			})
		});

		// Make the outer window draggable
		const header = html.find('header')[0];
		new Draggable(this, html, header, this.options.resizable);

		// Make the outer window minimizable
		if (this.options.minimizable) {
			header.addEventListener('dblclick', this._onToggleMinimize.bind(this));
		}

		// Set the outer frame z-index
		if (Object.keys(ui.windows).length === 0) _maxZ = 100;
		html.css({zIndex: ++_maxZ});

		// Return the outer frame
		return html;
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * not used anymore, but I also did not see it overridden anywhere, so I ignored this
	 */
	_injectHTML(html, options) {
		html.hide();
		$('body').append(html);
		this._element = html;
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * not used anymore, but I also did not see it overridden anywhere, so I ignored this
	 */
	_replaceHTML(element, html, options) {
		if (!element.length) return;

		// For pop-out windows update the inner content and the window title
		if (this.popOut) {
			element.find('.window-content').html(html);
			element.find('.window-title').text(this.title);
		}

		// For regular applications, replace the whole thing
		else {
			element.replaceWith(html);
		}
	}

	_setPopoutContent(title, innerHtml) {
		this.element.find('.window-title').text(title);
		this.element.find('.window-content').html(innerHtml);
	}

	/**
	 * Close the application and un-register references to it within UI mappings
	 * This function returns a Promise which resolves once the window closing animation concludes
	 * @return {Promise}
	 */
	async close() {
		let el = this.element;
		if (!el.length) return Promise.resolve();
		el.css({minHeight: 0});
		// NEW_MODULAR_RENDERER
		// removed so the performance testing makes more sense
		// return new Promise(resolve => {
		// 	el.slideUp(200, () => {
				el.remove();
				this._element = null;
				delete ui.windows[this.appId];
				this._rendered = false;
				// resolve();
			// });
		// });
	}

	/* -------------------------------------------- */

	/**
	 * Minimize the pop-out window, collapsing it to a small tab
	 * Take no action for applications which are not of the pop-out variety or apps which are already minimized
	 * @return {Promise}    A Promise which resolves to true once the minimization action has completed
	 */
	async minimize() {
		if (!this.popOut || [true, null].includes(this._minimized)) return;
		this._minimized = null;

		// Get content
		let window = this.element,
			header = window.find('.window-header'),
			content = window.find('.window-content');

		// Remove minimum width and height styling rules
		window.css({
			minWidth: 100,
			minHeight: 30
		});

		// Slide-up content
		content.slideUp(100);

		// Slide up window height
		return new Promise((resolve) => {
			window.animate({height: `${header[0].offsetHeight + 1}px`}, 100, () => {
				header.children().not(".window-title").not(".close").hide();
				window.animate({width: MIN_WINDOW_WIDTH}, 100, () => {
					window.addClass("minimized");
					this._minimized = true;
					resolve(true);
				});
			});
		})
	}

	/* -------------------------------------------- */

	/**
	 * Maximize the pop-out window, expanding it to its original size
	 * Take no action for applications which are not of the pop-out variety or are already maximized
	 * @return {Promise}    A Promise which resolves to true once the maximization action has completed
	 */
	async maximize() {
		if (!this.popOut || [false, null].includes(this._minimized)) return;
		this._minimized = null;

		// Get content
		let window = this.element,
			header = window.find('.window-header'),
			content = window.find('.window-content');

		// Expand window
		return new Promise((resolve) => {
			window.animate({
				width: this.position.width,
				height: this.position.height
			}, 100, () => {
				header.children().show();
				content.slideDown(100, () => {
					window.removeClass("minimized");
					this._minimized = false;
					window.css({
						minWidth: '',
						minHeight: ''
					});
					this.setPosition(this.position);
					resolve(true);
				});
			});
		})
	}

	/* -------------------------------------------- */

	/**
	 * Set the application position and store it's new location
	 * @param {Number} left
	 * @param {Number} top
	 * @param {Number} width
	 * @param {Number} height
	 */
	setPosition({left, top, width, height, scale} = {}) {
		let el = this.element[0],
			p = this.position,
			pop = this.popOut;

		// If Height is "auto" unset current preference
		if (this.options.height === "auto") {
			el.style.height = "auto";
			p.height = null;
		}

		// Update Width
		if (!el.style.width || width) {
			let minWidth = el.style.minWidth || pop ? MIN_WINDOW_WIDTH : 0;
			p.width = Math.clamped(
				minWidth,
				width || el.offsetWidth,
				el.style.maxWidth || (0.95 * window.innerWidth)
			);
			el.style.width = p.width + "px";
		}

		// Update Height
		if (!el.style.height || height) {
			let minHeight = el.style.minHeight || pop ? MIN_WINDOW_HEIGHT : 0;
			p.height = Math.clamped(
				minHeight,
				height || el.offsetHeight,
				el.style.maxHeight || (0.95 * window.innerHeight)
			);
			el.style.height = p.height + "px";
		}

		// Update Left
		if ((pop && !el.style.left) || Number.isFinite(left)) {
			let maxLeft = Math.max(window.innerWidth - el.offsetWidth, 0);
			if (!Number.isFinite(left)) left = (window.innerWidth - el.offsetWidth) / 2;
			p.left = Math.clamped(left, 0, maxLeft);
			el.style.left = p.left + "px";
		}

		// Update Top
		if ((pop && !el.style.top) || Number.isFinite(top)) {
			let maxTop = Math.max(window.innerHeight - el.offsetHeight, 0);
			if (!Number.isFinite(top)) top = (window.innerHeight - el.offsetHeight) / 2;
			p.top = Math.clamped(top, 0, maxTop);
			el.style.top = p.top + "px";
		}

		// Update Scale
		if (scale) {
			p.scale = scale;
			if (scale === 1) {
				el.style.transform = "";
			} else {
				el.style.transform = `scale(${scale})`;
			}
		}
	}


	/**
	 * Handle application minimization behavior - collapsing content and reducing the size of the header
	 * @param {Event} ev
	 * @private
	 */
	_onToggleMinimize(ev) {
		ev.preventDefault();
		if (this._minimized) {
			this.maximize(ev);
		} else {
			this.minimize(ev);
		}
	}

	/* -------------------------------------------- */

	/**
	 * Additional actions to take when the application window is resized
	 * @param {Event} event
	 * @private
	 */
	_onResize(event) {}
}

/**
 * NEW_MODULAR_RENDERER
 *
 * new base class for renderers
 */
if (!window.ApplicationRenderer) {
	window.ApplicationRenderer = class ApplicationRenderer {
		constructor() {
			this.postRenderFuncs = [];
		}

		/**
		 * Render the inner application content
		 *
		 * @param {jQuery} $toRenderOn    A jquery object that should be replaced with the rendered content
		 * @param {Object} data         The data used to render the inner template
		 * @return {Promise.<jQuery>}   A promise resolving to the constructed jQuery object
		 * @private
		 */
		async render($toRenderOn, data, options) {
			throw new Error(`Must be implemented`);
		}

		/**
		 * Function to run on the rendered HTML. Functions must be idempotent, i.e. calling the functions once or multiple
		 * times must yield exactly the same result (no duplicate listeners/behavior added etc)
		 *
		 * @param {function(jQuery): void} func
		 */
		addOnPostRender(func) {
			this.postRenderFuncs.push(func);
		}
	};
}


/**
 * NEW_MODULAR_RENDERER
 *
 * this is basically the old Application._renderInner method ported to a ApplicationRenderer
 */
if (!window.ApplicationRenderer) {
	window.HandlebarsRenderer = class HandlebarsRenderer extends ApplicationRenderer {

		constructor(options) {
			super();
			this.options = mergeObject(this.constructor.defaultOptions, options || {});
		}

		static get defaultOptions() {
			return {};
		}

		/**
		 * The path to the HTML template file which should be used to render the inner content of the app
		 * @type {String}
		 */
		get template() {
			return this.options.template;
		}

		async render($toRenderOn, data, options) {
			let html = await renderTemplate(this.template, data);
			if (html === "") throw new Error(`No data was returned from template ${this.template}`);
			let $elem = $(html);
			$toRenderOn.replaceWith($elem);
			this.postRenderFuncs.forEach(postRender => postRender($elem));
		}
	};
}

/**
 * NEW_MODULAR_RENDERER
 *
 * Ideally, the functionality of this class really should be provided by composition, not by inheritance.
 * The whole concept of this class doesn't really make sense anymore when we could have a Vue.js backend,
 * because the data model with bidirectional binding to the <input>s would take care of this.
 */
class ModularFormApplication extends ModularApplication {
	constructor(object, renderer, options) {
		super(renderer, options);

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * extracted from old overridden _renderInner
		 */
		this.renderer.addOnPostRender(($html) => {
			this.form = $html[0];
		});

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * old activateListeners(), made idempotent.
		 */
		this.renderer.addOnPostRender(($html) => {
			const form = $html[0];
			// Disable input fields if the form is not editable
			if (!this.isEditable) {
				this._disableFields(form);
				return;
			}

			// Process form submission
			form.onsubmit = this._onSubmit.bind(this);

			// Maybe process unfocus events
			if (this.options.submitOnUnfocus) {
				$html.find("input")
					.filter((idx, elem) => {
						const $elem = $(elem);
						if ($elem.data("input" + '_idempotent_marker')) {
							return false;
						}
						$elem.data("input" + '_idempotent_marker', true);
						return true;
					}).focusout(this._onUnfocus.bind(this));
			}

			// Detect and activate TinyMCE rich text editors
			$html.find('.editor-content[data-edit]')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('.editor-content[data-edit]' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('.editor-content[data-edit]' + '_idempotent_marker', true);
					return true;
				}).each((i, div) => this._activateEditor(div));

			// Detect and activate file-picker buttons
			$html.find('button.file-picker')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('button.file-picker' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('button.file-picker' + '_idempotent_marker', true);
					return true;
				}).each((i, button) => this._activateFilePicker(button));

			// Color change inputs
			$html.find('input[type="color"][data-edit]')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('input[type="color"][data-edit]' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('input[type="color"][data-edit]' + '_idempotent_marker', true);
					return true;
				}).change(this._onColorPickerChange.bind(this));
		});

		/**
		 * The object target which we are using this form to modify
		 * @type {*}
		 */
		this.object = object;

		/**
		 * A convenience reference to the form HTMLElement
		 * @type {HTMLElement}
		 */
		this.form = null;

		/**
		 * Keep track of any FilePicker instances which are associated with this form
		 * The values of this Array are inner-objects with references to the FilePicker instances and other metadata
		 * @type {Array}
		 */
		this.filepickers = [];

		/**
		 * Keep track of any mce editors which may be active as part of this form
		 * The values of this Array are inner-objects with references to the MCE editor and other metadata
		 * @type {Object}
		 */
		this.editors = {};
	}

	/* -------------------------------------------- */

	/**
	 * Assign the default options which are supported by the entity edit sheet
	 * @type {Object}
	 */
	static get defaultOptions() {
		const options = super.defaultOptions;
		options.classes = ["form"];
		options.closeOnSubmit = true;
		options.submitOnClose = false;
		options.submitOnUnfocus = false;
		options.editable = true;
		return options;
	}

	/* -------------------------------------------- */

	/**
	 * Is the Form Application currently editable?
	 * @type {Boolean}
	 */
	get isEditable() {
		return this.options.editable;
	}

	/* -------------------------------------------- */

	/**
	 * Provide data to the form
	 * @return {Object}   The data provided to the template when rendering the form
	 */
	getData() {
		return {
			object: duplicate(this.object),
			options: this.options
		}
	}

	/* -------------------------------------------- */


	/* -------------------------------------------- */

	/**
	 * A helper function to transform an HTML form into a FormData object which is ready for dispatch
	 * @param {HTMLElement} form    The form-type HTMLElement
	 * @return {FormData}           The prepared FormData object
	 * @private
	 */
	_getFormData(form) {
		const FD = new FormData(form);
		const dtypes = {};
		const editorTargets = Object.keys(this.editors);

		// Iterate over standard form data
		for (let k of FD.keys()) {
			let input = form[k];
			if (input.disabled) FD.delete(k);
			if (input.dataset.dtype) dtypes[k] = input.dataset.dtype;
		}

		// Explicitly handle all checkboxes in the form
		for (let box of form.querySelectorAll('input[type="checkbox"]')) {
			FD.set(box.name, box.checked || false);
			dtypes[box.name] = "Boolean";
		}

		// Process editable images
		for (let img of form.querySelectorAll('img[data-edit]')) {
			if (img.getAttribute("disabled")) continue;
			FD.set(img.dataset.edit, img.src.replace(window.location.origin + "/", ""));
		}

		// Process editable divs (excluding MCE editors)
		for (let div of form.querySelectorAll('div[data-edit]')) {
			if (div.getAttribute("disabled")) {
				continue;
			} else if (editorTargets.includes(div.dataset.edit)) continue;
			FD.set(div.dataset.edit, div.innerHTML.trim());
		}

		// Handle MCE editors
		Object.values(this.editors).forEach(ed => {
			if (ed.mce) {
				FD.delete(ed.mce.id);
				if (ed.changed) FD.set(ed.target, ed.mce.getContent());
			}
		});

		// Record target data types for casting
		FD._dtypes = dtypes;
		return FD;
	}

	/* -------------------------------------------- */
	/*  Event Listeners and Handlers                */

	/* -------------------------------------------- */


	/**
	 * If the form is not editable, disable its input fields
	 * @param form {HTMLElement}
	 * @private
	 */
	_disableFields(form) {
		const inputs = ["INPUT", "SELECT", "TEXTAREA", "BUTTON"];
		for (let i of inputs) {
			for (let el of form.getElementsByTagName(i)) el.setAttribute("disabled", "");
		}
	}

	/* -------------------------------------------- */

	/**
	 * Handle the change of a color picker input which enters it's chosen value into a related input field
	 * @private
	 */
	_onColorPickerChange(event) {
		event.preventDefault();
		let input = event.target,
			form = input.form;
		form[input.dataset.edit].value = input.value;
	}

	/* -------------------------------------------- */

	/**
	 * Handle standard form submission steps
	 * @param {Event} event           The submit event which triggered this handler
	 * @param {Boolean} preventClose  Override the standard behavior of whether to close the form on submit
	 * @returns {Promise}             A promise which resolves to the validated update data
	 * @private
	 */
	async _onSubmit(event, {preventClose = false} = {}) {
		event.preventDefault();
		if (!this._rendered || !this.options.editable || this._submitting) return false;
		this._submitting = true;

		// Acquire and validate Form Data
		const form = this.form;
		const formData = this._getFormData(form);

		// Construct update data object by casting form data
		const updateData = new Array(...formData.entries()).reduce((obj, [k, v]) => {
			let dt = formData._dtypes[k];
			if (dt === "Number") {
				obj[k] = Number(v);
			} else if (dt === "Boolean") {
				obj[k] = v === "true";
			} else {
				obj[k] = v;
			}
			return obj;
		}, {});

		// Trigger the object update
		await this._updateObject(event, updateData);
		if (this.options.closeOnSubmit && !preventClose) this.close();
		this._submitting = false;
		return updateData;
	}

	/* -------------------------------------------- */

	/**
	 * Handle unfocusing an input on form - maybe trigger an update if ``options.liveUpdate`` has been set to true
	 * @param event {Event}   The initial triggering event
	 * @private
	 */
	_onUnfocus(event) {
		this._submitting = true;
		setTimeout(() => {
			let hasFocus = $(":focus").length;
			if (!hasFocus) this._onSubmit(event);
			this._submitting = false;
		}, 25);
	}

	/* -------------------------------------------- */

	/**
	 * This method is called upon form submission after form data is validated
	 * @param event {Event}       The initial triggering submission event
	 * @param formData {Object}   The object of validated form data with which to update the object
	 * @returns {Promise}         A Promise which resolves once the update operation has completed
	 * @abstract
	 */
	async _updateObject(event, formData) {
		throw new Error("A subclass of the FormApplication must implement the _updateObject method.");
	}

	/* -------------------------------------------- */

	/*  TinyMCE Editor
	/* -------------------------------------------- */

	/**
	 * Activate a TinyMCE editor instance present within the form
	 * @param div {HTMLElement}
	 * @private
	 */
	_activateEditor(div) {

		// Get the editor content div
		let target = div.getAttribute("data-edit"),
			button = div.nextElementSibling,
			hasButton = button && button.classList.contains("editor-edit"),
			wrap = div.parentElement.parentElement,
			wc = $(div).parents(".window-content")[0];

		// Determine the preferred editor height
		let heights = [wrap.offsetHeight, wc ? wc.offsetHeight : null];
		if (div.offsetHeight > 0) heights.push(div.offsetHeight);
		let height = Math.min(...heights.filter(h => Number.isFinite(h)));

		// Get initial content
		const data = this.object instanceof Entity ? this.object.data : this.object,
			initialContent = getProperty(data, target);

		// Add record to editors registry
		this.editors[target] = {
			target: target,
			button: button,
			hasButton: hasButton,
			mce: null,
			active: !hasButton,
			changed: false
		};

		// Define editor options
		let editorOpts = {
			target: div,
			height: height,
			setup: mce => this.editors[target].mce = mce,
			save_onsavecallback: mce => {
				this._onEditorSave(target, mce.getElement(), mce.getContent());
				if (hasButton) {
					mce.remove();
					button.style.display = "block";
				}
			}
		};

		// Define the creation function
		const _createEditor = (target, editorOpts, initialContent) => {
			createEditor(editorOpts, initialContent).then(mce => {
				const editor = mce[0];
				editor.focus();
				editor.on('change', ev => this.editors[target].changed = true);
			});
		};

		// If we are using a toggle button, delay activation until it is clicked
		if (hasButton) {
			button.onclick = event => {
				this.editors[target].changed = false;
				this.editors[target].active = true;
				button.style.display = "none";
				editorOpts["height"] = div.offsetHeight;
				_createEditor(target, editorOpts, initialContent);

			};
		} else {
			_createEditor(target, editorOpts, initialContent);
		}
	}

	/* -------------------------------------------- */

	/**
	 * By default, when the editor is saved treat it as a form submission event
	 * @private
	 */
	_onEditorSave(target, element, content) {
		element.innerHTML = content;
		const formData = validateForm(this.form);
		let event = new Event("mcesave");

		// Remove the MCE from the set of active editors
		const editor = this.editors[target];
		editor.active = false;
		// NEW_MODULAR_RENDERER
		// I think the remaining <input>s may just have never been an issue with the handlebars
		// renderer because it throws away all HTML anyway
		const mce = editor.mce;
		const id = mce.id;
		const $form = $(mce.formElement);
		$form.find(`input[name="${id}"]`).remove();
		// END NEW_MODULAR_RENDERER
		mce.destroy();
		editor.mce = null;

		// Update the form object
		this._updateObject(event, formData);
	}

	/* -------------------------------------------- */

	/*  FilePicker UI
	/* -------------------------------------------- */

	/**
	 * Activate a FilePicker instance present within the form
	 * @param button {HTMLElement}
	 * @private
	 */
	_activateFilePicker(button) {
		button.onclick = event => {
			event.preventDefault();
			let target = button.getAttribute("data-target");
			let fp = FilePicker.fromButton(button);
			this.filepickers.push({
				target: target,
				app: fp
			});
			fp.browse();
		}
	}

	/* -------------------------------------------- */
	/*  METHODS                                     */

	/* -------------------------------------------- */

	/**
	 * Extend the logic applied when the application is closed to destroy any remaining MCE instances
	 * This function returns a Promise which resolves once the window closing animation concludes
	 * @return {Promise}
	 */
	async close() {
		if (!this._rendered) return;

		// Optionally trigger a save
		if (this.options.submitOnClose) this.submit();

		// Close any open FilePicker instances
		this.filepickers.forEach(fp => {
			if (fp.app) fp.app.close();
		});

		// Close any open MCE editors
		Object.values(this.editors).forEach(ed => {
			if (ed.mce) ed.mce.destroy();
		});
		this.editors = {};

		// Close the application itself
		return super.close();
	}

	/* -------------------------------------------- */

	/**
	 * Manually sumbmit the contents of a Form Application, processing its content as defined by the Application
	 * @returns {FormApplication}   Return a self-reference for convenient method chaining
	 */
	submit() {
		if (this._submitting) return;
		const submitEvent = new Event("submit");
		this._onSubmit(submitEvent, {preventClose: true});
		return this;
	}
}
