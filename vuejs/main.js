'use strict';

window.testdata = {};

/**
 * NEW_MODULAR_RENDERER
 *
 * Vue implementation of the ApplicationRenderer
 */
class VueRenderer extends ApplicationRenderer {

	constructor(options) {
		super();
		this.data = options.baseData || {};
		this.template = options.template;
	}

	async render($renderOn, data, options) {
		if (!this.vue) {
			this._createVue($renderOn, data);
		} else {
			this._updateVueData(data);
			if ($renderOn.length !== 1 || $renderOn[0] !== this.vue.$el) {
				$renderOn.replaceWith(this.vue.$el);
				this._runPostRenders(this.vue);
			}
		}
		return $(this.vue.$el);
	}

	_runPostRenders(vue) {
		console.log(`Running post render functions`);
		this.postRenderFuncs.forEach(func => func($(vue.$el)));
	}

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * this is just a very stupid implementation traversing the object and checking for === on non-objects.
	 * A "proper" Vue application would just simply update *only* the data that is actually changed.
	 * But it was much easier for me to just directly transfer the sheet implementation with minimal changes.
	 */
	_updateVueData(data) {
		function isObject(toTest) {
			return toTest !== null && typeof toTest === `object`;
		}

		const updateSubObjects = (subKey, curVue, curData) => {
			const subData = curData[subKey];
			const vueSubData = curVue[subKey];
			if (isObject(subData) && isObject(vueSubData)) {
				Object.keys(subData)
					.forEach((key) => {
						updateSubObjects(key, vueSubData, subData);
					});
			} else if (vueSubData !== subData) {
				curVue[subKey] = subData;
			}
		};
		Object.keys(data)
			.forEach((key) => {
				updateSubObjects(key, this.vue, data);
			});
	}

	_createVue($renderOn, data) {
		if ($renderOn.length > 1) {
			$renderOn.filter((idx, elem) => {
				let keep = idx === 0;
				if (!keep) {
					elem.remove();
				}
				return keep;
			});
		}

		const self = this;
		function runPostRenders() {
			self._runPostRenders(this);
		}

		this.vue = new Vue({
			el: $renderOn[0],
			data: mergeObject(this.data, data),
			template: this.template,
			updated: runPostRenders,
			mounted: runPostRenders
		});
	}
}

(() => {
	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * equivalent to the handlebars helper
	 */
	Vue.prototype.$numberFormat = (num, decimals = 0, sign = false) => {
		num = parseFloat(num).toFixed(decimals);

		if (sign) {
			return (num >= 0) ? `+` + num : num;
		} else {
			return num;
		}
	};

	Vue.prototype.$enrichHTML = enrichHTML;

	/**
	 * NEW_MODULAR_RENDERER
	 *
	 * Tinymce-editor component. There is actually an official tinymce vue.js component like this, but I'm
	 * not about to find out how to use it with foundry, as fvtt probably does all kinds of custom stuff.
	 *
	 * Doesn't work perfectly correctly when using it. But I'm not about to find out why, I'm just here to do a POC and measure performance.
	 * Initializing & basic usage work fine, and that's all I care about for difference between Vue & Handlebars
	 */
	Vue.component(`tinymce-editor`, {
		props: [`content`, `target`, `showButton`, `isOwner`, `isEditable`],
		template: ` <div v-if="typeof target !== 'undefined'" class="editor">
						<div class="editor-content" :data-edit="target" v-html="$enrichHTML(content, {secrets: isOwner, entities: true})"></div>
						<a v-if="showButton && isEditable" class="editor-edit"><i class="fas fa-edit"></i></a>
					</div>`
	});

	class VueJSSheet extends ModularActorSheet5eCharacter {
		constructor(actor, options) {
			super(actor, new VueRenderer({
				template: window.testTemplate
			}), {
				...options,
				classes: (options.classes || []).concat(["dnd5e", "actor", "character-sheet", "sheet"])
			});
		}

		getData() {
			const data = super.getData();
			// I'm not quite sure why this is needed, but `item.data.attuned.value` fails on `item.data.attuned` being undefined.
			// I think Handlebars might be more lenient and just evaluate that as `undefined`, but Vue templates are not
			for (const inventory of Object.values(data.actor.inventory)) {
				for (const item of inventory.items) {
					item.data.attuned = item.data.attuned || {};
				}
			}
			return data;
		}
	}

	Actors.registerSheet(`dnd5e`, VueJSSheet, {
		types: [`character`]
	});
})();


/**
 * NEW_MODULAR_RENDERER
 *
 * PERFORMANCE TESTS
 *
 * left click on foundry logo for handlebars, right-click for vue
 *
 * normal click is a performance test for an initial rendering
 *
 * ctrl+click is for updating a single value (in this case, hp from 0 to 1 and back)
 *
 * For this to work nicely, you have to make the following adjustments to foundry.js:
 *
 * search for `html.hide().fadeIn(200)` -> comment out
 *
 * search for `el.slideUp(200` -> comment out this line, and the following relatively: this - 1, this + 5, this + 6,
 * this + 7
 *
 * Also, adjust the entity IDs below for your test game. Because your test actors will have different IDs.
 */
Hooks.once(`ready`, () => {
	const handlebarsEntity = game.actors.get(`uAAlzljIluUrXqNG`);
	const vueEntity = game.actors.get(`7AoPS5XETOpeBNR5`);
	const testLoops = 200;
	const fractionDigits = 3;

	async function initialRender(entity) {
		await entity.sheet._render(true);
		await entity.sheet.close();
	}

	async function update(entity) {
		const curHp = entity.data.data.attributes.hp.value;
		const newHp = curHp === 0 ? 1 : 0;
		// call the private handler as we don't want a server update
		game.actors._updateEntity({
			updated: {
				_id: entity.id,
				[`data.attributes.hp.value`]: newHp
			},
			options: {},
			userId: game.user._id
		});
	}

	async function perfTest(name, entity, testFunc) {
		const start = performance.now();
		for (let i = 0; i !== testLoops; ++i) {
			await testFunc(entity);
		}
		const duration = performance.now() - start;
		alert(`${name} took ${duration.toFixed(fractionDigits)} ms for ${testLoops} renders, ` +
			`${(duration / testLoops).toFixed(fractionDigits)} ms per render`);
	}

	function perfTestHandler(name, entity) {
		return async (e) => {
			if (!e.ctrlKey) {
				console.log(`Starting ${name} initialRender test...`);
				await perfTest(name, entity, initialRender);
			} else {
				console.log(`Starting ${name} update test...`);
				await entity.sheet._render(true);
				await perfTest(name, entity, update);
			}
		};
	}

	const $logo = $(`#logo`);
	$logo.click(perfTestHandler('Handlebars', handlebarsEntity));
	$logo.contextmenu(perfTestHandler('Vue', vueEntity));
});
