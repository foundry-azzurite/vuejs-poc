class ModularActorSheet extends ModularBaseEntitySheet {
	constructor(...args) {
		super(...args);

		this.token = this.object.token;

		/**
		 * NEW_MODULAR_RENDERER
		 *
		 * replaces activateListeners(). Made idempotent.
		 */
		this.renderer.addOnPostRender((html) => {
			// Everything below is only needed if the sheet is editable
			if (!this.options.editable) return;

			// Update the sheet when we un-focus an input unless we have acquired focus on another input
			html.find("input")
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data("input" + '_update_handler')) {
						return false;
					}
					$elem.data("input" + '_update_handler', true);
					return true;
				}).focusout(this._onUnfocus.bind(this));

			// Update the sheet when a select field is changed
			html.find("select")
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data("select" + '_idempotent_marker')) {
						return false;
					}
					$elem.data("select" + '_idempotent_marker', true);
					return true;
				}).change(this._onSubmit.bind(this));

			// Make the Actor sheet droppable for Items
			this.form.ondragover = ev => this._onDragOver(ev);
			this.form.ondrop = ev => this._onDrop(ev);

			// Support Image updates
			html.find('img[data-edit="img"]')
				.filter((idx, elem) => {
					const $elem = $(elem);
					if ($elem.data('img[data-edit="img"]' + '_idempotent_marker')) {
						return false;
					}
					$elem.data('img[data-edit="img"]' + '_idempotent_marker', true);
					return true;
				}).click(ev => this._onEditImage(ev));
		});
		/**
		 * END NEW_MODULAR_RENDERER
		 */
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		options.template = "templates/sheets/actor-sheet.html";
		options.width = 720;
		options.height = 800;
		options.closeOnSubmit = false;
		options.submitOnClose = true;
		options.submitOnUnfocus = true;
		options.resizable = true;
		return options;
	}

	/* -------------------------------------------- */

	/**
	 * Define a unique and dynamic element ID for the rendered ActorSheet application
	 * @return {string}
	 */
	get id() {
		let id = `actor-${this.actor.id}`;
		if (this.token) id += `-${this.token.id}`;
		return id;
	}

	/* -------------------------------------------- */

	/**
	 * The displayed window title for the sheet - the entity name by default
	 * @type {String}
	 */
	get title() {
		return (this.token && !this.token.data.actorLink) ? `[Token] ${this.actor.name}` : this.actor.name;
	}

	/* -------------------------------------------- */

	/**
	 * A convenience reference to the Actor entity
	 * @type {Actor}
	 */
	get actor() {
		return this.object;
	}

	/* -------------------------------------------- */

	/**
	 * Prepare data for rendering the Actor sheet
	 * The prepared data object contains both the actor data as well as additional sheet options
	 */
	getData() {
		const data = super.getData();

		// Entity data
		data.actor = data.entity;
		data.data = data.entity.data;

		// Owned items
		data.items = data.actor.items;
		data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));
		return data;
	}

	/* -------------------------------------------- */

	/**
	 * Extend the Header Button configuration for the ActorSheet to add Token configuration buttons
	 * See Application._getHeaderButtons for documentation of the return Array structure.
	 * @return {Array.<Object>}
	 * @private
	 */
	_getHeaderButtons() {
		let buttons = super._getHeaderButtons();
		let canConfigure = this.options.editable && (game.user.isGM || (this.actor.owner && game.user.isTrusted));
		if (canConfigure) {
			buttons = [
				{
					label: "Sheet",
					class: "configure-sheet",
					icon: "fas fa-cog",
					onclick: ev => this._onConfigureSheet(ev)
				},
				{
					label: this.token ? "Token" : "Prototype Token",
					class: "configure-token",
					icon: "fas fa-user-circle",
					onclick: ev => this._onConfigureToken(ev)
				}
			].concat(buttons);
		}
		return buttons
	}

	/* -------------------------------------------- */

	/**
	 * Remove references to an active Token when the sheet is closed
	 * See Application.close for more detail
	 * @return {Promise}
	 */
	async close() {
		this.token = null;
		return super.close();
	}

	/* -------------------------------------------- */
	/*  Event Listeners                             */
	/* -------------------------------------------- */

	/* -------------------------------------------- */

	/**
	 * Handle requests to configure the prototype Token for the Actor
	 * @private
	 */
	_onConfigureToken(event) {
		event.preventDefault();

		// Determine the Token for which to configure
		const token = this.token || new Token(this.actor.data.token);

		// Render the Token Config application
		new TokenConfig(token, {
			left: Math.max(this.position.left - 560 - 10, 10),
			top: this.position.top,
			configureDefault: !this.token
		}).render(true);
	}

	/* -------------------------------------------- */

	/**
	 * Handle requests to configure the default sheet used by this Actor
	 * @private
	 */
	_onConfigureSheet(event) {
		event.preventDefault();
		new EntitySheetConfig(this.actor, {
			top: this.position.top + 40,
			left: this.position.left + ((this.position.width - 400) / 2)
		}).render(true);
	}

	/* -------------------------------------------- */

	/**
	 * Handle changing the actor profile image by opening a FilePicker
	 * @private
	 */
	_onEditImage(event) {
		new FilePicker({
			type: "image",
			current: this.actor.data.img,
			callback: path => {
				event.currentTarget.src = path;
				this._onSubmit(event);
			},
			top: this.position.top + 40,
			left: this.position.left + 10
		}).browse(this.actor.data.img);
	}

	/* -------------------------------------------- */

	/**
	 * Default handler for beginning a drag-drop workflow of an Owned Item on an Actor Sheet
	 * @param event
	 * @private
	 */
	_onDragItemStart(event) {
		const itemId = Number(event.currentTarget.dataset.itemId);
		const item = this.actor.getOwnedItem(itemId);
		event.dataTransfer.setData("text/plain", JSON.stringify({
			type: "Item",
			actorId: this.actor.id,
			data: item.data
		}));
	}

	/* -------------------------------------------- */

	/**
	 * Allow the Actor sheet to be a displayed as a valid drop-zone
	 * @private
	 */
	_onDragOver(event) {
		event.preventDefault();
		return false;
	}

	/* -------------------------------------------- */

	/**
	 * Handle dropped data on the Actor sheet
	 * @private
	 */
	_onDrop(event) {
		event.preventDefault();

		// Try to extract the data
		let data;
		try {
			data = JSON.parse(event.dataTransfer.getData('text/plain'));
			if (data.type !== "Item") return;
		} catch (err) {
			return false;
		}

		// Case 1 - Import from a Compendium pack
		if (data.pack) {
			this.actor.importItemFromCollection(data.pack, data.id);
		}

		// Case 2 - Data explicitly provided
		else if (data.data) {
			if (data.actorId === this.actor.id) {   // Sort existing items
				this._onSortItem(event, data.data);
			} else {
				this.actor.createOwnedItem(data.data);  // Create a new item
			}
		}

		// Case 3 - Import from World entity
		else {
			let item = game.items.get(data.id);
			if (!item) return;
			this.actor.createOwnedItem(item.data, true);
		}
		return false;
	}

	/* -------------------------------------------- */

	/*  Owned Item Sorting
	/* -------------------------------------------- */

	/**
	 * Handle a drop event for an existing Owned Item to sort that item
	 * @param {Event} event
	 * @param {Object} itemData
	 * @private
	 */
	_onSortItem(event, itemData) {

		// Get the drag source and its siblings
		const source = this.actor.getOwnedItem(Number(itemData.id));
		const siblings = this._getSortSiblings(source);

		// Get the drop target
		const dropTarget = event.target.closest(".item");
		const targetId = dropTarget ? Number(dropTarget.dataset.itemId) : null;
		const target = siblings.find(s => s.data.id === targetId);

		// Ensure we are only sorting like-types
		if (target && (source.data.type !== target.data.type)) return;

		// Perform the sort
		const sortUpdates = SortingHelpers.performIntegerSort(source, {
			target: target,
			siblings
		});
		const updateData = sortUpdates.map(u => {
			u.update.id = u.target.data.id;
			return u.update;
		});

		// Perform the update
		return this.actor.updateManyOwnedItem(updateData);
	}

	/* -------------------------------------------- */

	_getSortSiblings(source) {
		return this.actor.items.filter(i => (i.type === source.data.type) && (i.id !== source.data.id));
	}

	/* -------------------------------------------- */
}
